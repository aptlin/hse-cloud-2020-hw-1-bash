#!/usr/bin/env bash

awk '{ 
    total=$2+$3+$4
    average = total/3
    print $0 " : " (average > 50 ? average > 60 ? average > 80 ? "A" : "B" : "C" : "FAIL")
}'
