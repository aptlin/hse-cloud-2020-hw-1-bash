
#!/usr/bin/env bash

read total

curr_idx=$total
total_sum=0

while [ $curr_idx -gt 0 ]
do
   read curr
   total_sum=$((total_sum + curr))
   curr_idx=$((curr_idx - 1))     
done

printf "%.3f\n" `echo "$total_sum / $total" | bc -l` | head -c -1